<?php


namespace DiskoPete\LaravelAuthCookies\Contracts;


interface Config
{
    public function getExcludedPaths(): array;
}
