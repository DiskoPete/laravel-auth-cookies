<?php


namespace DiskoPete\LaravelAuthCookies\Models;


use DiskoPete\LaravelAuthCookies\Contracts\Config as ConfigContract;
use Illuminate\Contracts\Config\Repository;

class Config implements ConfigContract
{

    /**
     * @var Repository
     */
    private $configRepository;

    public function __construct(
        Repository $configRepository
    )
    {
        $this->configRepository = $configRepository;
    }

    public function getExcludedPaths(): array
    {
        return $this->configRepository->get(
            'auth-cookies.excludedPaths',
            []
        );
    }
}
