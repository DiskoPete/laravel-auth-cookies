<?php


namespace DiskoPete\LaravelAuthCookies\Http\Middleware;


use Closure;
use DiskoPete\LaravelAuthCookies\Contracts\Config;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Cookie;

class AuthCookiesMiddleware
{

    /**
     * @var Factory
     */
    private $authFactory;
    /**
     * @var CookieJar
     */
    private $cookieJar;
    /**
     * @var Config
     */
    private $config;

    public function __construct(
        CookieJar $cookieJar,
        Factory $authFactory,
        Config $config
    )
    {
        $this->authFactory = $authFactory;
        $this->cookieJar   = $cookieJar;
        $this->config      = $config;
    }

    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        if ($this->shouldProcessRequest($request)) {
            $this->queueCookies($request, $response);
        }

        return $response;
    }

    private function shouldProcessRequest(Request $request): bool
    {
        $process = true;

        foreach ($this->config->getExcludedPaths() as $pattern) {

            if (preg_match("/{$pattern}/", $request->path())) {
                $process = false;
                break;
            }

        }

        return $process;
    }

    private function queueCookies(Request $request, $response): void
    {
        $this
            ->makeCookies($request)
            ->each(fn(Cookie $cookie) => $response->headers->setCookie($cookie));
    }

    public function getCookieNames(): Collection
    {
        $holder = collect();

        foreach (config('auth.guards') as $guardId => $config) {
            $holder->put($guardId, $guardId . '_authenticated');
        }

        return $holder;
    }

    private function makeCookies(Request $request): Collection
    {
        $cookies = collect();

        foreach ($this->getCookieNames()->all() as $guardId => $name) {

            $guard         = $this->getGuard($guardId);
            $value         = $request->cookie($name);
            $authenticated = $guard->check();
            $cookie        = null;

            if ($authenticated) {
                $cookie = $this->cookieJar->make($name, 1, config('session.lifetime'), null, null, null, false);
            }

            if (!$authenticated && $value) {
                $cookie = $this->cookieJar->forget($name);
            }

            if (!$cookie) {
                continue;
            }

            $cookies->push($cookie);
        }

        return $cookies;
    }

    private function getGuard(string $guardId): Guard
    {
        return $this->authFactory->guard($guardId);
    }
}