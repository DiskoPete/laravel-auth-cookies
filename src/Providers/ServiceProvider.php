<?php

namespace DiskoPete\LaravelAuthCookies\Providers;

use DiskoPete\LaravelAuthCookies\Contracts\Config as ConfigContract;
use DiskoPete\LaravelAuthCookies\Http\Middleware\AuthCookiesMiddleware;
use DiskoPete\LaravelAuthCookies\Models\Config;
use Illuminate\Contracts\Http\Kernel as KernelContract;
use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                $this->getConfigPath() => config_path('auth-cookies.php')
            ]);
            return;
        }

        $this->exceptCookiesFromEncryption();
    }

    private function exceptCookiesFromEncryption(): void
    {
        foreach ($this->getKernel()->getMiddlewareGroups() as $middlewares) {

            foreach ($middlewares as $middleware) {

                if (!is_a($middleware, EncryptCookies::class, true)) {
                    continue;
                }

                $this->app->singleton($middleware);

                $middlewareInstance = $this->app->make($middleware);

                if (!$middlewareInstance instanceof EncryptCookies) {
                    continue;
                }

                $middlewareInstance->disableFor(
                    $this->getAuthCookieMiddleware()->getCookieNames()->values()->all()
                );

                return;
            }
        }
    }

    private function getAuthCookieMiddleware(): AuthCookiesMiddleware
    {
        return $this->app->make(AuthCookiesMiddleware::class);
    }

    public function register()
    {
        parent::register();

        $this->app->singleton(ConfigContract::class, Config::class);

        $this->registerMiddleware();
        $this->mergeConfigFrom($this->getConfigPath(), 'auth-cookies');
    }

    private function registerMiddleware()
    {
        $kernel = $this->getKernel();
        $kernel->pushMiddleware(AuthCookiesMiddleware::class);
    }

    private function getKernel(): KernelContract
    {
        return $this->app->make(KernelContract::class);
    }

    private function getConfigPath(): string
    {
        return __DIR__ . '/../../config/config.php';
    }
}
