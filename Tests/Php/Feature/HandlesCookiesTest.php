<?php


namespace DiskoPete\LaravelAuthCookies\Tests\Php\Feature;


use DiskoPete\LaravelAuthCookies\Contracts\Config;
use DiskoPete\LaravelAuthCookies\Tests\Php\TestCase;
use DiskoPete\LaravelAuthCookies\Tests\Php\Utils\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Route;
use Mockery\MockInterface;

class HandlesCookiesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function shouldAddLoggedInCookie(): void
    {
        $this->actingAs(factory(User::class)->create());

        self::assertTrue($this->isAuthenticated('web'));


        $this
            ->get('test')
            ->assertOk()
            ->assertCookie('web_authenticated');
    }

    /**
     * @test
     */
    public function shouldExpireCookieWhenNotLoggedIn(): void
    {

        self::assertFalse($this->isAuthenticated('web'));

        $cookieName = 'web_authenticated';

        $this
            ->withCookie($cookieName, 1)
            ->get('test')
            ->assertOk()
            ->assertCookieExpired($cookieName);
    }

    /**
     * @test
     */
    public function shouldExcludePathByConfig(): void
    {

        $this->mock(Config::class, function (MockInterface $config) {
            $config->shouldReceive('getExcludedPaths')->twice()->andReturn([
                '^test'
            ]);
        });
        $this->actingAs(factory(User::class)->create());

        self::assertTrue($this->isAuthenticated('web'));

        $cookieName = 'web_authenticated';

        $this
            ->get('test')
            ->assertOk()
            ->assertCookieMissing($cookieName);

        $this
            ->get('foo')
            ->assertOk()
            ->assertCookie($cookieName);
    }

    protected function setUp(): void
    {
        parent::setUp();

        Route::get('test', function () {
            return 'test';
        });

        Route::get('foo', function () {
            return 'test';
        });
    }
}