<?php

namespace DiskoPete\LaravelAuthCookies\Tests\Php;


class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->withoutExceptionHandling();
        $this->withFactories(__DIR__ . '/Utils/database/factories');
        $this->loadMigrationsFrom(__DIR__ . '/Utils/database/migrations');
    }


    protected function getPackageProviders($app)
    {
        return $this->readComposerJson()['extra']['laravel']['providers'];
    }

    private function readComposerJson(): array
    {
        $content = file_get_contents(__DIR__ . '/../../composer.json');

        return json_decode($content, true);
    }

}