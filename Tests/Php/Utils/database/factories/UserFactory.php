<?php
/**
 * @var Factory $factory
 */

use DiskoPete\LaravelAuthCookies\Tests\Php\Utils\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;


$factory->define(User::class, function (Faker $faker) {

    static $password;

    if (!$password) {
        $password = \Illuminate\Support\Facades\Hash::make('secret');
    }

    return [
        'name'  => $faker->name,
        'email' => $faker->email,
        'password' => $password
    ];
});
